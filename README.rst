==================
NoAds FileUnpacker
==================

**WARNING! This project has been abandoned long ago! See the** `reasons for abandonment`_ **below.**


About this program
==================
NoAds FileUnpacker is a tool to unpack files that are packed with TM FilePacker.

`TM FilePacker`_ (the link has been dead for years, see the `archived page`_) is a proprietary program, it uses its own proprietary archive format, runs only on Windows, installs an adware program called TM Agent and won't start if you uninstall TM Agent (maybe you can pay money instead, I'm not sure). Self-extracting TM FilePacker archives also install this adware when you run them.

NoAds FileUnpacker unpacks TM FilePacker archives without this hassle. It supports both .tmfp and self-extracting .exe archives (but, of course, only self-extracting archives that have been made with TM FilePacker), doesn't install any unwanted software and it's free and open source software, so you can download the source code, study how it works and modify it if you want to.

NoAds FileUnpacker is a Java program, it will run on any platform that supports Java, including Windows, Mac OS X, Linux and others.

Latest version of TM FilePacker at the time of creating NoAds was 2.1.7.216. Unfortunately, SPT files (created by old versions of TM FilePacker) are not supported.

The program has both a graphical and a command line interface.

.. _TM FilePacker: http://www.tmagency.net/tmfilepacker.html
.. _archived page: https://web.archive.org/web/20100612044719/http://tmagency.net/tmfilepacker.html

License
=======
BSD-like. See LICENSE.txt.


Reasons for abandonment
=======================
It seems the development of TM FilePacker has ceased several years ago and the program is no longer supported by its original developer. I haven't seen many TM FilePacker archives “in the wild” lately, so I haven't worked on this program since 2010 and I will no longer support it. The last published version remains available, in case someone needs it. It was previously hosted on Sourceforge.

There is no support whatsoever. All feature requests, bug reports, patches and pull requests will be rejected. You may fork the project and continue developing it on your own if you want.


Downloads
=========
**To use NoAds FileUnpacker you need Java Runtime Environment Version 6 or later!** Windows and Mac OS X users should use Oracle Java, available for free from https://www.java.com . Users of Linux or other OSes can try OpenJDK, available from your software repository or at http://openjdk.java.net . At least OpenJDK Version 8 seems to work fine.

**Download the program itself:** `version 0.1.1`_

To modify the program, you will also need the source code. It is available here_. 

**IMPORTANT NOTE:** if the Help window within the program appears extremely small, simply drag its borders to make it bigger.

Unfortunately, the program's source code has never been stored in a version control system, so it's only available as a .zip archive.

.. _version 0.1.1: https://bitbucket.org/Acharvak/noadsunpacker/downloads/noadsunpacker-0.1.1.zip
.. _here: https://bitbucket.org/Acharvak/noadsunpacker/downloads/noadsunpacker-0.1.1-src.zip
